#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

#Find out your monitor name with xrandr or arandr (save and you get this line)
#xrandr --output VGA-1 --primary --mode 1360x768 --pos 0x0 --rotate normal
#xrandr --output DP2 --primary --mode 1920x1080 --rate 60.00 --output LVDS1 --off &
#xrandr --output LVDS1 --mode 1366x768 --output DP3 --mode 1920x1080 --right-of LVDS1
#xrandr --output HDMI2 --mode 1920x1080 --pos 1920x0 --rotate normal --output HDMI1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output VIRTUAL1 --off

#change your keyboard if you need it
#setxkbmap fr

#starting utility applications at boot time
run nm-applet &
slimbookbattery --minimize &
picom &
nitrogen --restore &
xset s 500 &
#xss-lock -l /usr/bin/i3lock-fancy &
#xss-lock -l /usr/bin/i3lock-fancy &
xss-lock -l -n dim-screen.sh -- i3lock-fancy &
#blueberry-tray &
blueman-applet &
nextcloud --background &
#protonmail-bridge --no-window &
caffeine &
systemctl restart --user pipewire.service
volumeicon &
#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &


#starting user applications at boot time
#run pamac-tray &
#numlockx on &
#run discord &
#run caffeine -a &
#run vivaldi-stable &
#run firefox &
#run thunar &
#run dropbox &
#run insync start &
#run spotify &
#run atom &
