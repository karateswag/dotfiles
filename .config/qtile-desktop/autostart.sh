#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

#Find out your monitor name with xrandr or arandr (save and you get this line)
#xrandr --output VGA-1 --primary --mode 1360x768 --pos 0x0 --rotate normal

#change your keyboard if you need it
#setxkbmap -layout fr

xsetroot -cursor_name left_ptr &

#starting utility applications at boot time
run nm-applet &
run volumeicon &
picom &
nitrogen --restore &
#xss-lock -l /usr/bin/i3lock-fancy &
xss-lock -l xsecurelock &
numlockx on &
caffeine &
nextcloud --background &
deluge-gtk &
/usr/lib/xfce4/notifyd/xfce4-notifyd &


#starting user applications at boot time
#protonmail-bridge --no-window &
#blueberry-tray &
#run pamac-tray &
#run discord &
#run caffeine -a &
#run vivaldi-stable &
#run firefox &
#run thunar &
#run dropbox &
#run insync start &
#run spotify &
#run atom &
