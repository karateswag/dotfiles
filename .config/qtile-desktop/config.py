import os
import re
import socket
import subprocess
from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.log_utils import logger

mod = "mod4"
terminal = guess_terminal()

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html

    # Run application
        Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    Key([mod], "d", lazy.spawn("dmenu_run -f -l 5 -nb '#12151f' -nf '#ffffff' -sb '#ff7edb' -fn 'DejaVu Sans:size=12'"),
        desc="Launch dmenu"),

    Key([mod, "shift"], "Return", lazy.spawn("thunar"),
        desc="Launch thunar"),

    Key(["mod1", "control"], "f", lazy.spawn("firefox"),
        desc="Launch firefox"),

    Key(["mod1", "control"], "t", lazy.spawn("thunderbird"),
        desc="Launch thunderbird"),

    Key(["mod1", "control"], "j", lazy.spawn("joplin-desktop"),
        desc="Launch joplin-desktop"),

    Key(["mod1", "control"], "g", lazy.spawn("gimp"),
        desc="Launch gimp"),

    Key(["mod1", "control"], "r", lazy.spawn(terminal + " -e ranger"),
        desc="ranger"),

    Key(["mod1", "control"], "e", lazy.spawn(terminal + " -e nvim"),
        desc="nvim"),

    Key(["mod1", "control"], "h", lazy.spawn(terminal + " -e htop"),
        desc="htop"),

    Key(["mod1", "control"], "v", lazy.spawn("vlc"),
        desc="vlc"),

    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),
    Key([mod], "Tab", lazy.layout.next(),
        desc="Switch window focus to other pane(s) of stack"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # RESIZE UP, DOWN, LEFT, RIGHT
    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),

    # FULLSCREEN
    Key([mod], "f", lazy.window.toggle_fullscreen()),

    # Move to next screen
    Key([mod], "space", lazy.next_screen(), desc="move to next screen"),

    # Kill focused window
    Key([mod], "a", lazy.window.kill(), desc="Kill focused window"),

    # Toggle between different layouts as defined below
    Key([mod, "control"], "space", lazy.next_layout(), desc="Toggle between layouts"),

    # Reload config & quit qtile
    Key([mod, "shift"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),

    Key([mod], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),

    # MULTIMEDIA KEYS

    # INCREASE/DECREASE/MUTE VOLUME
#    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
#    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q set Master 5%-")),
#    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q set Master 5%+")),
#    Key([], "XF86AudioMicMute", lazy.spawn("amixer -q set Capture toggle")),

    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),
    Key([], "XF86AudioStop", lazy.spawn("playerctl stop")),

    # SCREENSHOTS
    Key([], "Print", lazy.spawn('flameshot full -p /home/victor/Pictures/Screenshots')),
    Key(["shift"], "Print", lazy.spawn('flameshot gui')),

]

################################################################################
### GROUPS #####################################################################
################################################################################

groups = [
        Group("1", label="", layout="monadtall", matches=[Match(wm_class=[""])]),
        Group("2", label="", layout="monadtall", matches=[Match(wm_class=["firefox"])]),
        Group("3", label="", layout="monadtall", matches=[Match(wm_class=["Thunderbird"])]),
        Group("4", label="異", layout="monadtall", matches=[Match(wm_class=["Gimp","Gimp-2.10","Atom"])]),
        Group("5", label="", layout="monadtall", matches=[Match(wm_class=["Joplin","libreoffice*","libreoffice-startcenter","libreoffice-writer","libreoffice-calc"])]),
        Group("6", label="", layout="max", matches=[Match(wm_class=["vlc"])]),
        Group("7", label="", layout="monadtall", matches=[Match(wm_class=["Remote-viewer","VirtualBox Manager"])]),
        Group("8", label="", layout="monadtall", matches=[Match(wm_class=["dota2", "overwatch.exe"])]),
        Group("9", label="", layout="monadtall", matches=[Match(wm_class=["discord","Steam","Lutris"])]),
        Group("0", label="", layout="monadtall", matches=[Match(wm_class=["Thunar"])]),
]

# List Key for groups
k = ["ampersand", "eacute", "quotedbl", "apostrophe", "parenleft", "minus", "egrave", "underscore", "ccedilla", "agrave"]
# List screen for groups
s = [0, 1, 1, 0, 1, 0, 0, 0, 1, 1]

for index, i in enumerate(groups):
    keys.extend([
        # CHANGE WORKSPACE
        Key([mod], k[index], lazy.group[i.name].toscreen(s[index]), lazy.to_screen(s[index])),

        # MOVE WINDOWS TO SELECTED WORKSPACE AND FOLLOW FOCUS
        #Key([mod, "shift"], k[index], lazy.window.togroup(i.name), lazy.group[i.name].toscreen(s[index]), lazy.to_screen(s[index])),
        # MOVE WINDOWS TO SELECTED WORKSPACE
        Key([mod, "shift"], k[index], lazy.window.togroup(i.name)),
    ])


layouts = [
    layout.MonadTall(margin=8, border_with=2, border_focus="#ff7edb", border_normal="#262335", ratio=0.51, single_border_width=0, single_margin=0),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    #layout.Columns(border_focus_stack=['#d75f5f', '#8f3d3d'], border_width=4),
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

################################################################################
#### BARS ######################################################################
################################################################################

# COLORS FOR THE BAR

def init_colors():
    return [
            ["#12151f", "#12151f"], # color 0 - Black
            ["#fe4450", "#fe4450"], # color 1 - Red
            ["#60ff60", "#60ff60"], # color 2 - Green
            ["#fede5d", "#fede5d"], # color 3 - Yellow
            ["#80a0ff", "#80a0ff"], # color 4 - Blue
            ["#ff7edb", "#ff7edb"], # color 5 - Magenta
            ["#03edf9", "#03edf9"], # color 6 - Cyan
            ["#ffffff", "#ffffff"], # color 7 - White
            ["#888690", "#888690"], # color 8 - Bright Black
            ["#e55a5e", "#e55a5e"], # color 9 - Bright Red
            ["#72f1b8", "#72f1b8"], # color 10 - Bright Green
            ["#ea9652", "#ea9652"], # color 11 - Bright Yellow
            ["#99b3ff", "#99b3ff"], # color 12 - Bright Blue
            ["#d884c6", "#d884c6"], # color 13 - Bright Magenta
            ["#9af7fc", "#9af7fc"], # color 14 - Bright Cyan
            ["#d4d3d7", "#d4d3d7"], # color 15 - Bright White
    ]

colors = init_colors()

# WIDGETS FOR THE BAR

widget_defaults = dict(
    font='DejaVuSans Nerd Font',
    fontsize=13,
    padding=4,
    background=colors[0],
    foreground=colors[1],

)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(
                    linewidth = 0,
                    padding = 5,
                    ),
                widget.GroupBox(
                    font = "FontAwesome",
                    fontsize = 30,
                    margin_x = 5,
                    margin_y = 3,
                    padding_x = 8,
                    padding_y = 1,
                    borderwidth = 2,
                    active = colors[5],
                    inactive = colors[10],
                    highlight_color = colors[1],
                    highlight_method = "border",
                    this_current_screen_border = colors[6],
                    this_screen_border = colors[5],
                    other_current_screen_border = colors[5],
                    other_screen_border = colors[5],
                    disable_drag = True,
                    rounded = True,
                    ),
                widget.CurrentLayoutIcon(
                    scale = 0.5,
                    ),
                widget.Sep(
                    foreground = colors[0],
                    background= "#12151f",
                    linewidth = 2,
                    padding = 5,
                    size_percent = 40,
                    ),
                widget.Prompt(),
                widget.WindowName(
                    background= "#12151f",
                    foreground = colors[5],
                    fontsize = 18,
                    ),
                widget.CurrentScreen(
                    font = "FontAwesome",
                    fontsize = 26,
                    margin_x = 0,
                    margin_y = 3,
                    padding_x = 3,
                    padding_y = 5,
                    active_color = colors[6],
                    active_text = "",
                    inactive_color = colors[8],
                    inactive_text = "",
                    ),
                widget.Sep(
                    foreground = colors[0],
                    linewidth = 2,
                    padding = 5,
                    size_percent = 40,
                    ),
                widget.Systray(
                    icon_size = 32,
                    padding = 10,
                    ),
                widget.Sep(
                    foreground = colors[0],
                    linewidth = 2,
                    padding = 5,
                    size_percent = 40,
                    ),
                widget.Clock(
                    foreground = colors[10],
                    format = '%a %d-%m-%Y',
                    fontsize = 18,
                    ),
                widget.Clock(
                    foreground = colors[10],
                    format = '%H:%M',
                    fontsize = 18,
                    ),
                widget.Sep(
                    linewidth = 0,
                    padding = 5,
                    ),
            ],
            50,
        ),
    ),
    Screen(
        top=bar.Bar(
            [
                widget.Sep(
                    linewidth = 0,
                    padding = 5,
                    ),
                widget.GroupBox(
                    font = "FontAwesome",
                    fontsize = 22,
                    margin_x = 2,
                    margin_y = 3,
                    padding_x = 5,
                    padding_y = 1,
                    borderwidth = 2,
                    active = colors[5],
                    inactive = colors[10],
                    highlight_color = colors[1],
                    highlight_method = "border",
                    this_current_screen_border = colors[6],
                    this_screen_border = colors[5],
                    other_current_screen_border = colors[5],
                    other_screen_border = colors[5],
                    disable_drag = True,
                    rounded = True,
                    ),
                widget.CurrentLayoutIcon(
                    scale = 0.5,
                    ),
                widget.Sep(
                    foreground = colors[0],
                    background= "#12151f",
                    linewidth = 2,
                    padding = 5,
                    size_percent = 40,
                    ),
                widget.WindowName(
                    background= "#12151f",
                    foreground = colors[5],
                    fontsize = 14,
                    ),
                widget.CurrentScreen(
                    font = "FontAwesome",
                    fontsize = 26,
                    margin_x = 0,
                    margin_y = 3,
                    padding_x = 3,
                    padding_y = 5,
                    active_color = colors[6],
                    active_text = "",
                    inactive_color = colors[8],
                    inactive_text = "",
                    ),
                widget.Sep(
                    foreground = colors[0],
                    linewidth = 2,
                    padding = 5,
                    size_percent = 40,
                    ),
                widget.CPU(
                    foreground = colors[14],
                    format = 'CPU {load_percent}%',
                    ),
                widget.ThermalSensor(
                    foreground = colors[14],
                    tag_sensor = "Tctl",
                    fmt = '{}',
                    ),
                widget.NvidiaSensors(
                    foreground = colors[2],
                    format = '{temp}°C',
                    ),
                widget.Memory(
                    foreground = colors[3],
                    format = '{MemUsed: .2f}{mm} /{MemTotal: .1f}{mm}',
                    measure_mem = 'G'
                    ),
                widget.Sep(
                    foreground = colors[0],
                    linewidth = 2,
                    padding = 5,
                    size_percent = 40,
                    ),
                widget.Clock(
                    foreground = colors[10],
                    format = '%a %d-%m-%Y',
                    ),
                widget.Clock(
                    foreground = colors[10],
                    format = '%H:%M',
                    ),
                widget.Sep(
                    linewidth = 0,
                    padding = 5,
                    ),
            ],
            30,
        ),
    ),

]


# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(border_width=2, border_focus="#8d62a9", border_normal="#282A36",
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class='confirmreset'),  # gitk
        Match(wm_class='makebranch'),  # gitk
        Match(wm_class='maketag'),  # gitk
        Match(wm_class='ssh-askpass'),  # ssh-askpass
        Match(wm_class='Pavucontrol'),  # pavucontrol
        Match(wm_class='Galculator'),  # Galculator
        Match(title='branchdialog'),  # gitk
        Match(title='pinentry'),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "focus" #or "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
